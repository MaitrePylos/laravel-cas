<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSheldonColumnsToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            if (!Schema::hasColumn('user', 'id_people')) {
                Schema::table('user', function (Blueprint $table) { // TODO create before created_at
                    $table->integer('id_people')->nullable()->comment("Id de l'utilisateur au sein de Sheldon");
                    $table->integer('numproeco')->nullable()->comment("Matricule de la personne");
                    $table->string('firstname')->nullable();
                    $table->string('lastname')->nullable();
                });
            }
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropColumn('id_people');
            $table->dropColumn('numproeco');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
        });
    }
}
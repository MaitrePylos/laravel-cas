<?php namespace Maitrepylos\Cas;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Maitrepylos\Cas\Middleware\Authenticate;
use Maitrepylos\Cas\Middleware\ChooseLoginUrl;
use Maitrepylos\Cas\Middleware\RedirectIfAuthenticated;

class CasServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../src/Config/cas.php' => config_path('cas.php'),
        ]);

        $this->publishes([
            __DIR__ . '/../src/database/migrations/add_sheldon_to_user.php' => database_path('migrations/add_sheldon_to_user.php'),
        ]);

        $this->publishes([
            __DIR__ . '/../src/Http/Controllers/Auth/CasController.php' => app_path('Http/Controllers/Auth/CasController.php'),
        ]);

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('cas.auth', Authenticate::class);
        $router->aliasMiddleware('cas.guest', RedirectIfAuthenticated::class);
        $router->aliasMiddleware('chose_login', ChooseLoginUrl::class);


    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('cas', function ($app) {
            return new CasManager($app);
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('cas');
    }
}

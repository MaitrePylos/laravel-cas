<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Maitrepylos\Cas\Facades\Cas;
use Maitrepylos\Cas\Traits\AfterCasAuth;

class CasController extends Controller
{
    use AfterCasAuth;


    public function callback()
    {
        return redirect()->route('home');
    }

}
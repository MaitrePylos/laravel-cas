<?php
/**
 * PHP versions 7.4
 * @Copyright Formatux.be.
 * @Author: G² <info@formatux.be>
 * Date: 27/08/20
 * Description :
 *
 */

namespace Maitrepylos\Cas\Middleware;

class ChooseLoginUrl
{
    use Closure;

    /**
     * Redirect to auth.henallux if option in .env is enabled
     * We can bypass auth with url /login?ignore
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('cas.cas_enable') && !$request->exists('ignore')) {
            if ($request->getRequestUri() == '/login') {
                return redirect()->route('cas.login');
            } else if ($request->getRequestUri() == '/logout') {
                return redirect()->route('cas.logout');
            }
        }
        return $next($request);
    }
}
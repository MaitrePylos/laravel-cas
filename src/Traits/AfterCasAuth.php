<?php

namespace Maitrepylos\Cas\Traits;

use Illuminate\Support\Facades\Auth as authentification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Maitrepylos\Cas\ApiSheldon;
use Maitrepylos\Cas\Facades\Cas;

trait AfterCasAuth
{
    // attributes
    //  public function getAttributes($name)
    public function getAttributes()
    {
        return Cas::user()->getAttributes();
    }

    public function getAttribute($name)
    {
        $value = Cas::user()->getAttribute($name);
        if (is_null($value))
            Log::warning("[AUTH] l'attribut $name n'a pas été envoyé par le CAS");
        return $value;
    }

    public function getEmail()
    {
        return Cas::user()->id;
    }

    public function getUsername()
    {
        // Modification de la fonctiond de Denis, car le dns peut-être autre que Henallux.be
        [$name, $dns] = explode('@', Cas::user()->id);
        return $name;
    }


    public function __get($name)
    {
        return $this->getAttribute($name);
    }

    //user
    public function createUser($check_if_exist = false)
    {
        $user_class = config('auth.providers.users.model');
        // get basic datas
        $email = $this->getEmail();
        $username = $this->getUsername();
        $password = Hash::make(env('MAGIC_PASSWORD'));
        $user = ($check_if_exist === true) ? $user_class::where('email', $email)->first() : false;
        if (!$user) {
            $user = new $user_class();
            $user->email = $email;
            $user->name = $username;
            $user->password = $password;
        }
        return $user;
    }

    public function saveUser($user)
    {
        $user->save();
        Log::debug("[USER] Création/Mise à jour de {$user->email} dans la base de données locale");
    }

    /**
     *
     * @param $user
     * @param bool $save_or_not
     * @return mixed
     */
    public function addNumproecoToUser($user, $save_or_not = false)
    {
        // data for rqs

        $identity = $this->getUsername();


        $url = 'people?select=matricule&identifiant=eq.' . $identity;

        return $this->_addSheldonDatasToUser($user, $url, $save_or_not);
    }

    public function addPeopleIdToUser($user, $save_or_not = false)
    {
        // data for rqs
        $identity = $this->getUsername();
        $url = 'people?select=id_people&identifiant=eq.' . $identity;

        return $this->_addSheldonDatasToUser($user, $url, $save_or_not);
    }

    public function addNamesToUser($user, $save_or_not = false)
    {
        // data for rqs
        $identity = $this->getUsername();

        $url = 'people?select=nom,prenom&identifiant=eq.' . $identity;

        return $this->_addSheldonDatasToUser($user, $url, $save_or_not);
    }

    // auth
    public function authInLaravel($user)
    {
        if (!authentification::check())
            authentification::login($user);
    }

    public function authInBackpack($user)
    {
        if ($user->can('backpack_access'))
            Auth::guard(backpack_guard_name())->login($user); // auth in backpack too
    }

    // - // AfterCasAuthPorail risque d'avoir le mm besoin !! -> make SheldonApiTrait  ?

    protected function _addSheldonDatasToUser($user, $url, $save_or_not = false)
    {
        // rqs to shedlon
        $datas = $this->_getSheldonApiDatas($url);
        // parse result
        $translated = [
            'matricule' => 'numproeco',
            'prenom' => 'firstname',
            'nom' => 'lastname',
        ];
        if ($datas) {
            foreach ($datas as $key => $value) {
                $key = (array_key_exists($key, $translated)) ? $translated[$key] : $key;
                $user->$key = $value;
            }
        }
        // save in user table or not
        if ($save_or_not)
            $user->save();
        // end
        return $user;
    }

    protected function _getSheldonApiDatas($url, $method = 'GET'):?array
    {

        $client = ApiSheldon::getClientSheldon();
        $options = ApiSheldon::getOptionsSheldon($client['token']);
        $response = ApiSheldon::getRequestSheldon($client['client'], $method, $url, $options);

        return $response;

    }
}







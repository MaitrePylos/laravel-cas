<?php
/**
 * PHP versions 5.6
 * @Copyright Formatux.be.
 * @Author: G² <info@formatux.be>
 * Date: 27/08/20
 * Description :
 *
 */

namespace Maitrepylos\Cas;


use Symfony\Component\HttpClient\HttpClient;

class ApiSheldon
{
    public static function getClientSheldon():array
    {
        $client = HttpClient::create();
        $token = $client->request('POST', config('cas.cas_url_api') . 'rpc/login',
            ['body' => ["pseudo" => config('cas.cas_pseudo_api'), "pass" => config('cas.cas_pass_api')]]);
        return ['client'=>$client,'token'=> $token->toArray()[0]['token']];
    }

    public static function getOptionsSheldon($token):array
    {
        return ['headers' =>
            ['Prefer' => 'count=exact', 'Authorization' => 'Bearer ' . $token]
        ];
    }

    public static function getRequestSheldon($client,$method,$url,$options):?array
    {
        $response = $client->request($method, config('cas.cas_url_api') . $url, $options);
        return $response->toArray()[0] ?? null;
    }
}